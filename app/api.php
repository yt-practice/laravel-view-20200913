<?php
require_once __DIR__ . '/config.php';

header('content-type: application/json');

$method = filter_input(INPUT_GET, 'method');

switch ($method) {
  case 'pagelist':
    $result = Models\File::where('type', 'file')->get()
      ->filter(function($p) { return !preg_match('#^(layout|parts)/#', $p->slug); })
      ->pluck('slug');
    exit(json_encode(compact('result')));
    break;

  default:
    # code...
    break;
}

$error = 'not found';
http_response_code(404);
exit(json_encode(compact('error')));
