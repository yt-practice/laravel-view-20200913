<?php

require_once __DIR__ . '/vendor/autoload.php';

date_default_timezone_set('Asia/Tokyo');
error_reporting(E_ALL);
ini_set('display_errors', 'On');

use Dotenv\Dotenv;
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

define('DB_DRIVER', 'mysql');
define('DB_HOST', $_ENV['DB_HOST']);
define('DB_DATABASE', $_ENV['DB_DATABASE']);
define('DB_USERNAME', $_ENV['DB_USERNAME']);
define('DB_PASSWORD', $_ENV['DB_PASSWORD']);
define('DB_CHARSET', $_ENV['DB_CHARSET']);
define('DB_COLLATION', $_ENV['DB_COLLATION']);
define('DB_PREFIX', '');

use Models\DB\Connection;
Connection::init();

function h(string $in = null) {
  if (null === $in) return '';
  return htmlspecialchars($in, ENT_QUOTES, 'UTF-8');
}

function view(string $temp, array $params = []) {
  $blade = new Models\Blade;
  return $blade->make($temp, $params)->render();
}
