<?php
require_once __DIR__ . '/config.php';

$page = filter_input(INPUT_GET, 'page') ?? 'homepage';

try {
  Models\File::first();
} catch (\Throwable $th) {
  sleep(5);
  require_once __DIR__ . '/scripts/create_tables.php';
}

$get_default = function ($title) {
  $title = str_replace("'", "\\'", $title);
  return <<<"HTML"
@extends('layout.common')
@section('title', '$title')
@include('layout.header')
@section('content')
<div> {{ \$name }} </div>
@endsection
@include('layout.footer')
HTML;
};

if ('form' === $page) {
  $target = filter_input(INPUT_GET, 'target') ?? filter_input(INPUT_POST, 'target') ?? 'homepage';
  $html = filter_input(INPUT_POST, 'html') ?? null;
  if (is_string($html) and is_string($target)) {
    $file = Models\File::where('path', "root/$target.blade.php")->first();
    if ($file) {
      $file->contents = $html;
      $file->saveWithLog();
    } else {
      exit(view('not_found'));
    }
  } else {
    $file = Models\File::where('path', "root/$target.blade.php")->first();
    if (!$file) {
      exit(view('not_found'));
    }
    $html = $file->contents ?? $get_default($target);
  }

  exit(view('form', ['target' => $target, 'html' => $html]));
}

if ('create-page' === $page) {
  $target = filter_input(INPUT_POST, 'page-slug') ?? null;
  $pagename = filter_input(INPUT_POST, 'page-name') ?? null;

  if ($target and is_string($target)) {
    if (!($pagename = @trim($pagename ?? ''))) {
      $pagename = basename($target);
    }
    $mkdirp = function($p) use (&$mkdirp) {
      if (!$p) return;
      $d = Models\File::where('path', $p)->first();
      if (!$d and $mkdirp(dirname($p))) {
        $d = new Models\File([
          'path' => $p,
          'type' => 'dir',
          'contents' => '',
        ]);
        return $d->saveWithLog();
      }
      if ($d and $d->type === 'dir') return true;
    };
    $file = new Models\File([
      'path' => "root/$target.blade.php",
      'type' => 'file',
      'contents' => $get_default($pagename),
    ]);
    if (!$mkdirp(dirname($file->path))) {
      throw new Exception("Error Processing Request", 1);
    }
    $file->saveWithLog();
    $page = 'form';
    http_response_code(302);
    header('location: /?' . http_build_query(compact('page', 'target')));
    exit;
  }
}

try {
  $name = filter_input(INPUT_GET, 'name') ?? 'テスト太郎';
  exit(view($page, ['name' => $name]));
} catch (\InvalidArgumentException $th) {
  $message = $th->getMessage();
  if (preg_match('/^View \[.*?\] not found\.$/mu', $message)) {
    http_response_code(404);
    exit(view('not_found', ['message' => $message]));
  }
  throw $th;
}
