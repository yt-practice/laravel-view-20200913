<?php
namespace Models;

/**
 * 参考: https://github.com/jenssegers/blade
 */

use Illuminate\Contracts\View\Factory as FactoryContract;
use Illuminate\Contracts\View\View;
use Illuminate\Events\Dispatcher;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\CompilerEngine;

use League\Flysystem\Filesystem as FlysystemFilesystem;
use Illuminate\Filesystem\FilesystemAdapter;

class BladeCompiler2 extends BladeCompiler {
  function __construct($files, $cachePath) {
    $this->files = $files;
    $this->cachePath = $cachePath;
  }
}

class ViewFinder2 extends FileViewFinder {
  function __construct($files, array $paths, array $extensions = null) {
    $this->files = $files;
    $this->paths = \array_map([$this, 'resolvePath'], $paths);
    if (isset($extensions)) {
      $this->extensions = $extensions;
    }
  }
}

class Blade implements FactoryContract {
  /**
   * @var Factory
   */
  private $factory;

  /**
   * @var BladeCompiler
   */
  private $compiler;

  public function __construct() {
    $viewPaths = ['root'];
    $cachePath = '/tmp/fs-adapter-caches';

    $events = new Dispatcher;
    $filesystem = new FilesystemAdapter(new FlysystemFilesystem(new FsAdapter));
    $viewFinder = new ViewFinder2($filesystem, $viewPaths);
    $resolver = new EngineResolver;
    $this->factory = new Factory($resolver, $viewFinder, $events);
    $this->compiler = new BladeCompiler2($filesystem, $cachePath);

    $resolver->register('blade', function () {
      return new CompilerEngine($this->compiler);
    });
  }

  public function render(string $view, array $data = [], array $mergeData = []): string {
    return $this->make($view, $data, $mergeData)->render();
  }

  public function make($view, $data = [], $mergeData = []): View {
    return $this->factory->make($view, $data, $mergeData);
  }

  public function compiler(): BladeCompiler {
    return $this->compiler;
  }

  public function directive(string $name, callable $handler) {
    $this->compiler->directive($name, $handler);
  }

  public function if($name, callable $callback) {
    $this->compiler->if($name, $callback);
  }

  public function exists($view): bool {
    return $this->factory->exists($view);
  }

  public function file($path, $data = [], $mergeData = []): View {
    return $this->factory->file($path, $data, $mergeData);
  }

  public function share($key, $value = null) {
    return $this->factory->share($key, $value);
  }

  public function composer($views, $callback): array {
    return $this->factory->composer($views, $callback);
  }

  public function creator($views, $callback): array {
    return $this->factory->creator($views, $callback);
  }

  public function addNamespace($namespace, $hints): self {
    $this->factory->addNamespace($namespace, $hints);

    return $this;
  }

  public function replaceNamespace($namespace, $hints): self {
    $this->factory->replaceNamespace($namespace, $hints);

    return $this;
  }

  public function __call(string $method, array $params) {
    return \call_user_func_array([$this->factory, $method], $params);
  }
}
