<?php
namespace Models\DB;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

class Connection {
	static private $inited = false;
	static private $capsule;
	static function get() {
		return self::$capsule
			->schema()
			->getConnection()
			->getPdo();
	}
	static function capsule() {
		return self::$capsule;
	}
	static function init() {
		if (self::$inited) return;

		$capsule = new Capsule;

		$capsule->addConnection([
			'driver'    => DB_DRIVER,
			'host'      => DB_HOST,
			'database'  => DB_DATABASE,
			'username'  => DB_USERNAME,
			'password'  => DB_PASSWORD,
			'charset'   => DB_CHARSET,
			'collation' => DB_COLLATION,
			'prefix'    => DB_PREFIX,
		]);

		$capsule->setEventDispatcher(new Dispatcher(new Container));
		$capsule->setAsGlobal();
		$capsule->bootEloquent();

		$capsule
			->schema()
			->getConnection()
			->getPdo()
			->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);

		self::$capsule = $capsule;
		self::$inited = true;
	}

	static function prjs($data, $key = '') {
		\register_shutdown_function(function() use ($data, $key) {
			// if (self::$silent) return;
			$js = \json_encode($data);
			if (!isset($_SESSION)) {
				\print_r(\json_decode($js));
				echo PHP_EOL;
				return;
			}
			if ($key) $js = \json_encode("{$key} => %o") . ', ' . $js;
			foreach (\headers_list() as $line)
				if (\preg_match('$^content-type:\\s*(text|application)/json$mui', $line)) return;
			\printf('<script>console.log(%s)</script>', $js);
		});
	}

	static function debug_listensqls($with_bindings = false) {
		$queries = [];
		\register_shutdown_function(function() use (&$queries) {
			self::prjs($queries, 'queries');
		});
		\Illuminate\Database\Capsule\Manager::schema()
			->getConnection()
			->listen(function(\Illuminate\Database\Events\QueryExecuted $query) use (&$queries, $with_bindings) {
				$queries[] = $with_bindings ? [$query->sql, $query->bindings] : $query->sql;
			});
	}
	
}
