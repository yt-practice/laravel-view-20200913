<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model {
  protected $fillable = [
    'path',
    'contents',
    'type',
    'mimetype',
  ];
  protected $casts = [
    'size' => 'int',
  ];
  function changeLogs() {
    return $this->hasMany(FileChangeLog::class);
  }
  function saveWithLog() {
    $r = $this->save();
    $log = new FileChangeLog([
      'path' => $this->path,
      'contents' => $this->contents,
      'type' => $this->type,
      'mimetype' => $this->mimetype,
      'file_id' => $this->id,
      'logtype' => 'save',
    ]);
    $log->save();
    return $r;
  }
  function deleteWithLog() {
    $r = $this->delete();
    $log = new FileChangeLog([
      'path' => $this->path,
      'contents' => $this->contents,
      'type' => $this->type,
      'mimetype' => $this->mimetype,
      'file_id' => $this->id,
      'logtype' => 'delete',
    ]);
    $log->save();
    return $r;
  }
  function getSlugAttribute() {
    return substr($this->path, 5, -10);
  }
}
