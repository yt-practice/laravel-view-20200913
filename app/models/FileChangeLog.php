<?php
namespace Models;
use Illuminate\Database\Eloquent\Model;

class FileChangeLog extends Model {
  protected $fillable = [
    'path',
    'contents',
    'type',
    'mimetype',
    'file_id',
    'logtype',
  ];
  protected $casts = [
    'size' => 'int',
    'file_id' => 'int',
  ];
  function file() {
    return $this->belongsTo(File::class);
  }
}
