<?php

namespace Models;

use Models\File;

/**
 * 参考: https://github.com/IntegralSoftware/flysystem-pdo-adapter
 */

use League\Flysystem\Adapter\Local;

use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Adapter\Polyfill\NotSupportingVisibilityTrait;
use League\Flysystem\Config;
use League\Flysystem\Util;


class FsAdapter extends AbstractAdapter {

  static $dump_log = true;
  static $_call_log = [];
  protected $forCache;

  use NotSupportingVisibilityTrait;

  function __construct() {
    $this->setPathPrefix('');
    $structure = '/';
    $this->forCache = new Local(
      $structure,
      \LOCK_EX,
      Local::DISALLOW_LINKS,
      [
        'file' => [
          'public' => 0755,
          'private' => 0755,
        ],
        'dir' => [
          'public' => 0755,
          'private' => 0755,
        ],
      ]
    );
  }

  /**
   * Check whether a file exists.
   *
   * @param string $path
   *
   * @return array|bool|null
   */
  public function has($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->has($path);
    $path = $this->applyPathPrefix($path);
    return File::where('path', $path)->exists();
  }

  /**
   * Read a file.
   *
   * @param string $path The path to the file.
   *
   * @return array|false
   */
  public function read($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->read($path);
    $path = $this->applyPathPrefix($path);
    $file = File::where('path', $path)->first() ?? false;
    if (false === $file) return false;
    return $this->mapFileInfo($file);
  }

  /**
   * Retrieves a read-stream for a path.
   *
   * @param string $path The path to the file.
   *
   * @return array|false
   */
  public function readStream($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->readStream($path);
    $path = $this->applyPathPrefix($path);
    $file = File::where('path', $path)->first() ?? false;
    if (false === $file) return false;
    $arr = $this->mapFileInfo($file);
    $stream = \fopen('php://memory','r+');
    \fwrite($stream, $file->contents);
    \rewind($stream);
    $arr['stream'] = $stream;
    return $arr;
  }

  protected function listContentsImpl($directory = '', $recursive = false) {
    $dir = \preg_quote($directory);
    $reg = $recursive ? "^{$dir}/" : "^{$dir}/[^/]+$";
    return File::where('path', 'REGEXP', $reg)->get();
  }

  /**
   * List contents of a directory.
   *
   * @param string $directory
   * @param bool   $recursive
   *
   * @return array
   */
  public function listContents($directory = '', $recursive = false) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->listContents($directory, $recursive);
    $directory = $this->applyPathPrefix($directory);
    if (File::where('path', $directory)->where('type', 'dir')->doesntExist()) {
      return [];
    }
    return $this->listContentsImpl($directory, $recursive)
      ->map(function($a) { return $this->mapFileInfo($a); })
      ->all();
  }

  /**
   * Get all the meta data of a file or directory.
   *
   * @param string $path
   *
   * @return array|false
   */
  public function getMetadata($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->getMetadata($path);
    $path = $this->applyPathPrefix($path);
    $file = File::where('path', $path)->first() ?? false;
    if (false === $file) return false;
    return $this->mapFileInfo($file);
  }

  /**
   * Get the size of a file.
   *
   * @param string $path
   *
   * @return array|false
   */
  public function getSize($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->getSize($path);
    return $this->getMetadata($path);
  }

  /**
   * Get the mimetype of a file.
   *
   * @param string $path
   *
   * @return array|false
   */
  public function getMimetype($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->getMimetype($path);
    return $this->getMetadata($path);
  }

  /**
   * Get the last modified time of a file as a timestamp.
   *
   * @param string $path
   *
   * @return array|false
   */
  public function getTimestamp($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->getTimestamp($path);
    return $this->getMetadata($path);
  }

  /**
   * Write a new file.
   *
   * @param string $path
   * @param string $contents
   * @param Config $config   Config object
   *
   * @return array|false false on failure file meta data on success
   */
  public function write($path, $contents, Config $config) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->write($path, $contents, $config);
    $type = 'file';
    $mimetype = Util::guessMimeType($path, $contents);
    $path = $this->applyPathPrefix($path);
    $file = new File(\compact('path', 'contents', 'type', 'mimetype'));
    try {
      if ($file->saveWithLog()) return $this->mapFileInfo($file);
    } catch (\Throwable $th) {
      if (self::$dump_log) throw $th;
    }
    return false;
  }

  /**
   * Write a new file using a stream.
   *
   * @param string   $path
   * @param resource $resource
   * @param Config   $config   Config object
   *
   * @return array|false false on failure file meta data on success
   */
  public function writeStream($path, $resource, Config $config) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->writeStream($path, $resource, $config);
    $contents = \stream_get_contents($resource);
    return $this->write($path, $contents, $config);
  }

  /**
   * Update a file.
   *
   * @param string $path
   * @param string $contents
   * @param Config $config   Config object
   *
   * @return array|false false on failure file meta data on success
   */
  public function update($path, $contents, Config $config) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->update($path, $contents, $config);
    $type = 'file';
    $mimetype = Util::guessMimeType($path, $contents);
    $path = $this->applyPathPrefix($path);
    try {
      if ($file = File::where('path', $path)->where('type', 'file')->first()) {
        $file->mimetype = $mimetype;
        $file->contents = $contents;
        if ($file->saveWithLog()) return $this->mapFileInfo($file);
      }
    } catch (\Throwable $th) {
      if (self::$dump_log) throw $th;
    }
    return false;
  }

  /**
   * Update a file using a stream.
   *
   * @param string   $path
   * @param resource $resource
   * @param Config   $config   Config object
   *
   * @return array|false false on failure file meta data on success
   */
  public function updateStream($path, $resource, Config $config) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->updateStream($path, $resource, $config);
    $contents = \stream_get_contents($resource);
    return $this->update($path, $contents, $config);
  }

  /**
   * Rename a file.
   *
   * @param string $path
   * @param string $newpath
   *
   * @return bool
   */
  public function rename($path, $newpath) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->rename($path, $newpath);
    $path2 = $this->applyPathPrefix($path);
    try {
      if ($item = File::where('path', $path2)->first()) {
        $item->path = $this->applyPathPrefix($newpath);
        $item->saveWithLog();
        if ('dir' === $item->type) {
          $pathLength = \strlen($path);
          $list = $this->listContentsImpl($path2);
          foreach ($list as $object) {
            $tmppath = $this->getFilePath($object);
            $object->path = $this->applyPathPrefix($newpath . \substr($tmppath, $pathLength));
            $object->saveWithLog();
          }
        }
        return true;
      }
    } catch (\Throwable $th) {
      if (self::$dump_log) throw $th;
    }
    return false;
  }

  /**
   * Copy a file.
   *
   * @param string $path
   * @param string $newpath
   *
   * @return bool
   */
  public function copy($path, $newpath) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->copy($path, $newpath);
    $path2 = $this->applyPathPrefix($path);
    try {
      if ($item = File::where('path', $path2)->first()) {
        $item2 = new File([
          'path' => $this->applyPathPrefix($newpath),
          'contents' => $item->contents,
          'type' => $item->type,
          'mimetype' => $item->mimetype,
        ]);
        if ($item2->saveWithLog()) return true;
      }
    } catch (\Throwable $th) {
      if (self::$dump_log) throw $th;
    }
    return false;
  }

  /**
   * Delete a file.
   *
   * @param string $path
   *
   * @return bool
   */
  public function delete($path) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->delete($path);
    $path2 = $this->applyPathPrefix($path);
    if ($item = File::where('path', $path2)->where('type', 'file')->first()) {
      return (bool) $item->deleteWithLog();
    }
    return false;
  }

  /**
   * Delete a directory.
   *
   * @param string $dirname
   *
   * @return bool
   */
  public function deleteDir($dirname) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->deleteDir($dirname);
    $path2 = $this->applyPathPrefix($path);
    if ($item = File::where('path', $path2)->where('type', 'dir')->first()) {
      return (bool) $item->deleteWithLog();
    }
    return false;
  }

  /**
   * Create a directory.
   *
   * @param string $dirname directory name
   * @param Config $config
   *
   * @return array|false
   */
  public function createDir($dirname, Config $config) {
    if (0 === \strpos($path, 'tmp/')) return $this->forCache->createDir($dirname, $config);
    $type = 'dir';
    $path = $this->applyPathPrefix($path);
    $contents = '';
    $file = new File(\compact('path', 'contents', 'type'));
    try {
      if ($file->saveWithLog()) return $this->mapFileInfo($file);
    } catch (\Throwable $th) {
      if (self::$dump_log) throw $th;
    }
    return false;
  }

  protected function getFilePath(File $file) {
    return $this->removePathPrefix($file['path']);
  }

  protected function mapFileInfo(File $file) {
    $path = $this->getFilePath($file);
    $normalized = [
      'type' => $file->type,
      'path' => $path,
      'timestamp' => $file->updated_at->getTimestamp(),
      'dirname' => Util::dirname($path),
    ];
    if ($normalized['type'] === 'file') {
      $normalized['size'] = $file->size;
      $normalized['mimetype'] = $file->mimetype;
      $normalized['contents'] = $file->contents;
    }
    return $normalized;
  }


}
