@extends('layout.common')
@section('title', 'Not Found')
@include('layout.header')
@section('content')
<p style="color: red">{{ $message ?? 'Not Found...' }}</p>
@endsection
@include('layout.footer')
