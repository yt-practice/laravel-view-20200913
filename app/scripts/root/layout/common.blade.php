<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>@yield('title')</title>
</head>
<body>
@yield('header')
@yield('content')
@yield('footer')
</body>
</html>