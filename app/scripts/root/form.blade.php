@extends('layout.common')
@section('title', '編集')
@include('layout.header')
@section('content')
<form method="post">
<p>{{ $target . '.blade.php' }}</p>
<input type="hidden" name="target" value="{{ $target }}">
@include('parts/editor', ['name' => 'html', 'value' => $html])
<button type="submit">save</button>
</form>
@endsection
@include('layout.footer')
