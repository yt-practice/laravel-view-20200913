<div id="container" style="height:400px;border:1px solid black;"></div>
<textarea name="{{ $name }}" style="display: none">{{ $value ?? '' }}</textarea>
<script src="https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.20.0/min/vs/loader.min.js"></script>
<script>
(() => {
  require.config({
    paths: { 'vs': 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.20.0/min/vs' },
    'vs/nls': { availableLanguages: { '*': 'ja' } },
  })
  window.MonacoEnvironment = { getWorkerUrl: () => proxy }
  const proxy = URL.createObjectURL(new Blob([`
    self.MonacoEnvironment = {
      baseUrl: 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.20.0/min'
    }
    importScripts('https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.20.0/min/vs/base/worker/workerMain.min.js')
  `], { type: 'text/javascript' }))
  require(["vs/editor/editor.main"], () => {
    const editor = monaco.editor.create(document.getElementById('container'), {
      value: <?=json_encode($value ?? '')?>,
      language: <?=json_encode($language ?? 'php')?>,
    })
    editor.onDidChangeModelContent(() => {
      const e = document.querySelector('[name=<?=json_encode($name)?>]')
      if (e) e.value = editor.getValue()
    })
    editor.addAction({
      id: 'add-anchor-link',
      label: 'ページリンクの追加',
      contextMenuGroupId: 'insert-text',
      contextMenuOrder: 1.5,
      async run(ed) {
        const wrap = document.createElement('wrap')
        document.body.appendChild(wrap)
        const modal = document.createElement('div')
        wrap.appendChild(modal)
        modal.style.zIndex = '999999999'
        modal.style.position = 'fixed'
        modal.style.top = '20px'
        modal.style.right = '20px'
        modal.style.left = '20px'
        modal.style.padding = '30px'
        modal.style.background = '#fff'
        const back = document.createElement('div')
        wrap.appendChild(back)
        back.style.zIndex = '999999990'
        back.style.position = 'fixed'
        back.style.top = '0'
        back.style.right = '0'
        back.style.left = '0'
        back.style.bottom = '0'
        back.style.background = '#000'
        back.style.opacity = '0.5'

        const res = await fetch('/api.php?method=pagelist').then(r => r.json()).catch(x => {
          document.body.removeChild(wrap)
          return Promise.reject(x)
        })

        modal.innerHTML = `<form>
          <select></select>
          <button type="submit">ok</button>
          <button type="button" data-cancel>cancel</button>
        </form>`

        const slug = await new Promise(done => {
          const form = modal.querySelector('form')
          const select = modal.querySelector('select')
          for (const slug of res.result) {
            const opt = document.createElement('option')
            opt.value = slug
            opt.textContent = slug
            select.appendChild(opt)
          }
          form.addEventListener('submit', e => {
            e.preventDefault()
            done(select.value)
          })
          modal.querySelector('[data-cancel]').addEventListener('click', () => {
            done(null)
          })
          back.addEventListener('click', () => {
            done(null)
          })
          select.focus()
        }).finally(() => {
          document.body.removeChild(wrap)
        })

        if (slug) {
          const sel = ed.getSelection();
          const range = new monaco.Range(sel.startLineNumber, sel.startColumn, sel.endLineNumber, sel.endColumn)
          const identifier = { major: 1, minor: 1 }
          const sp = new URLSearchParams()
          sp.append('page', slug)
          const text = `<a href="/?${sp}">${slug}</a>\n`
          const op = { identifier, range, text, forceMoveMarkers: true }
          ed.executeEdits(undefined, [op])
        }

        return null
      }
    })
  })
})()
</script>
