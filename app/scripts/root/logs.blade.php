@extends('layout.common')
@section('title', 'logs')
@include('layout.header')
@section('content')
<?php

$list = Models\FileChangeLog::get();
$code = json_encode($list, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

?>
<pre><code>{{ $code }}</code></pre>
@endsection
@include('layout.footer')
