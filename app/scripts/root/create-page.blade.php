@extends('layout.common')
@section('title', 'ページ作成')
@include('layout.header')
@section('content')
<form method="post">
<div>
slug:
<input name="page-slug" pattern="^[a-zA-Z0-9_-/]+$" type="text">
</div>
<div>
name:
<input name="page-name" type="text">
</div>
<button type="submit">create</button>
</form>
@endsection
@include('layout.footer')
