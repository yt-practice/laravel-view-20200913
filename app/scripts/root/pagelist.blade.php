@extends('layout.common')
@section('title', 'ページ一覧')
@include('layout.header')
@section('content')
<?php

$items = Models\File::where('type', 'file')->get()->pluck('slug');

?>
<ul>
  @foreach ($items as $page)
  <li>
    {{ $page }}
    @if (preg_match('#^(layout|parts)/#', $page))
    <del>view</del>
    @else
    <a href="?{{ http_build_query(compact('page')) }}">view</a>
    @endif
    <a href="?{{ http_build_query(['page' => 'form', 'target' => $page]) }}">edit</a>
  </li>
  @endforeach
</ul>
@endsection
@include('layout.footer')
