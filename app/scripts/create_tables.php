<?php
require_once __DIR__ . '/../config.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

use Models\File;

$schema = Capsule::schema();

$schema->dropAllTables();

$delTree = function ($dir) use (&$delTree) {
  $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      is_dir("$dir/$file") ? $delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
  };
@$delTree('/tmp/fs-adapter-caches');
@mkdir('/tmp/fs-adapter-caches', 0777, true);
@chmod('/tmp/fs-adapter-caches', 0777);

try {
  if (!$schema->hasTable('files')) {
    $schema->create('files', function (Blueprint $table) {
      $table->increments('id');
      $table->string('path', 255);
      $table->text('contents');
      $table->enum('type', ['dir', 'file']);
      $table->string('mimetype', 127)->nullable();
      $table->timestamps();
      $table->softDeletes();
      $table->unsignedInteger('is_deleted')
        ->storedAs('CASE WHEN deleted_at IS NOT NULL THEN CAST(deleted_at AS UNSIGNED) ELSE 0 END');
      $table->unsignedInteger('size')->storedAs('LENGTH(contents)');
      $table->unique(['path', 'is_deleted']);
    });
  }

  if (!$schema->hasTable('file_change_logs')) {
    $schema->create('file_change_logs', function (Blueprint $table) {
      $table->increments('id');
      $table->string('path', 255);
      $table->text('contents');
      $table->enum('type', ['dir', 'file']);
      $table->string('mimetype', 127)->nullable();
      $table->unsignedInteger('file_id');
      $table->enum('logtype', ['save', 'delete']);
      $table->timestamps();
      $table->foreign('file_id')
        ->references('id')->on('files')
        ->onDelete('restrict')->onUpdate('cascade');
    });
  }

  $scan = function(string $p) use (&$scan) {
    $root = new File([
      'path' => $p,
      'type' => 'dir',
      'contents' => '',
    ]);
    $root->save();
    foreach (scandir(__DIR__ . '/' . $p) as $name) {
      if ('.' === $name or '..' === $name) continue;
      $n = $p . '/' . $name;
      if (is_dir(__DIR__ . '/' . $n)) $scan($n);
      elseif (is_file(__DIR__ . '/' . $n) and preg_match('/\.php$/mu', $name)) {
        $file = new File([
          'path' => $n,
          'type' => 'file',
          'contents' => file_get_contents(__DIR__ . '/' . $n),
        ]);
        $file->save();
      }
    }
  };
  $scan('root');

} catch (\Throwable $th) {
  \var_dump($th);
}

