# ビューを mysql で管理するテスト

1. `blade.php` == PHP を mysql 上に置く
2. php にコンパイルして `/tmp` に置く
3. include する

# データベース構造

```sh
# データベース初期化
php scripts/create_tables.php
```

```sql
-- ファイル・ディレクトリ
create table `files` (
  `id` int unsigned not null auto_increment primary key,
  `path` varchar(255) not null,
  `contents` text not null,
  `type` enum('dir', 'file') not null,
  `mimetype` varchar(127) null,
  `created_at` timestamp null,
  `updated_at` timestamp null,
  `deleted_at` timestamp null,
  `is_deleted` int unsigned as (CASE WHEN deleted_at IS NOT NULL THEN CAST(deleted_at AS UNSIGNED) ELSE 0 END) stored,
  `size` int unsigned as (LENGTH(contents)) stored,
  unique `files_path_is_deleted_unique`(`path`, `is_deleted`)
);

-- ファイル・ディレクトリの変更履歴
create table `file_change_logs` (
  `id` int unsigned not null auto_increment primary key,
  `path` varchar(255) not null,
  `contents` text not null,
  `type` enum('dir', 'file') not null,
  `mimetype` varchar(127) null,
  `file_id` int unsigned not null,
  `logtype` enum('save', 'delete') not null,
  `created_at` timestamp null,
  `updated_at` timestamp null
); 
alter table `file_change_logs` add constraint `file_change_logs_file_id_foreign`
  foreign key (`file_id`) references `files` (`id`) on delete restrict on update cascade;
```
